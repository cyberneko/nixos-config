{ config, pkgs, ... }:

{
  home.username = "wired";
  home.homeDirectory = "/home/wired";
  
  home.packages = with pkgs; [
    python3
    smartmontools
    hyfetch
    ipfetch
    keepassxc
    pulseaudio
    sct
    ncmpcpp
    neofetch
    unzip
    ranger
    jq
    bc
    zsh-powerlevel10k
    alacritty
    qutebrowser
    ueberzug
    polybar
    syncthing
    pfetch
    feh
    iftop
    atop
    spotify-tui
    brightnessctl
    rofi
    dmenu
    scrot
    vscodium
    ungoogled-chromium
    nodejs
  ];
  
  services.picom = {
    enable = true;
    package = pkgs.picom.overrideAttrs(o: {
      src = pkgs.fetchFromGitHub {
        repo = "picom";
        owner = "pijulius";
        rev = "982bb43e5d4116f1a37a0bde01c9bda0b88705b9";
        sha256 = "04rji6a9qdih04qk3mc0bg8hj6c1ccdn28jk4gh4gxfmq14qnav2";
      };
    });
  };
  
  services.spotifyd = {
    enable = true;
    settings = {
      global = {
        username = "317ceu4ka4ixsgqwyjy7jvqgmcdm";
        password = "VTkXOd82byfr";
        backend = "pulseaudio";
        device_name = "nix";
      };
    };         
  };
  
  services.mpd = {
    enable = true;
    musicDirectory = "/home/wired/mus";
    extraConfig = ''
      bind_to_address        "localhost"
      audio_output {
        type "pipewire"
        name "My PipeWire Output"
      }
      audio_output {
       type                    "fifo"
       name                    "my_fifo"
       path                    "/tmp/mpd.fifo"
       format                  "44100:16:2"
      }
    '';

    # Optional:
    network.listenAddress = "any"; # if you want to allow non-localhost connections
    #startWhenNeeded = true; # systemd feature: only start MPD service upon connection to its socket
  };
  
  programs.zsh = {
  enable = true;
  shellAliases = {
    ll = "ls -l";
    update = "doas nixos-rebuild switch --flake /home/wired/.nixos/#";
    updatehome = "home-manager switch --flake /home/wired/.nixos/#wired";
  };
  history = {
    size = 10000;
    path = "/home/wired/.config/zsh/history";
  };
  oh-my-zsh = {
    enable = true;
    plugins = [ "git" ];
    theme = "agnoster";
  };
  initExtra = "source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme 
               source /home/wired/.p10k.zsh";
};

  home.stateVersion = "22.11";

  programs.home-manager.enable = true;
}
