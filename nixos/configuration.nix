# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  impermanence = builtins.fetchTarball {
    url = "https://github.com/nix-community/impermanence/archive/master.tar.gz";
    sha256 = "1xj0s2qj7xcq1zai6diymfza7mxwhizw1akxz392nv39az71yn24";
  };
in

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      "${impermanence}/nixos.nix"
    ];
  
  environment.persistence."/persist/system" = { 
    directories = [
      "/etc/nixos"
      "/var/log"
      "/var/lib"
    ]; 
    files = [
      "/etc/machine-id"
      "/etc/nix/id_rsa"
      "/etc/wpa_supplicant.conf"
      "/etc/ssh/ssh_host_rsa_key"
      "/etc/ssh/ssh_host_rsa_key.pub"
      "/etc/ssh/ssh_host_ed25519_key"
      "/etc/ssh/ssh_host_ed25519_key.pub"
    ];
  };
  
  nix.settings.allowed-users = [ "@wheel" ];  
  nix.settings.experimental-features = [ "nix-command" "flakes" ];  
  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = true;

  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.supportedFilesystems = [ "btrfs" ];
  hardware.enableAllFirmware = true;
  nixpkgs.config.allowUnfree = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.luks.devices = {
      cryptNix = {
        # Use https://nixos.wiki/wiki/Full_Disk_Encryption
        device = "/dev/disk/by-uuid/dd6982e5-0d87-4db7-9e28-1cfa8116ccdb";
        preLVM = true;
      };
  };  
  
  networking.hostName = "cyberia"; # Define your hostname.
  # Pick only one of the below networking options.
  networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = false;  # Easiest to use and most distros use this by default.
  
  # boot.kernelParams = [ "quiet" ];
  boot.consoleLogLevel = 3;
  
  # Set your time zone.
  time.timeZone = "Asia/Almaty";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  users.users.wired.shell = pkgs.zsh;
  users.defaultUserShell = pkgs.zsh;
  i18n.defaultLocale = "en_US.UTF-8";

  services.xserver = {
    enable = true;
    layout = "us,ru";
    xkbModel = "pc105";
    xkbOptions = "grp:alt_shift_toggle";
#    videoDrivers = [ "nvidia" ];
    libinput = {
      enable = true;
    };
    
    windowManager = {
      xmonad = {
        enable = true;
        enableContribAndExtras = true;
      };
    };
  
    displayManager = {
      defaultSession = "none+xmonad";
      startx = {
        enable = true;
      };
    };  
  };
 
  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "FiraCode" ]; })
  ];  

  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;
  };
  
  users.mutableUsers = false;
  users.users.root.initialHashedPassword = "$6$Ws6qNpMALcpvQ7yh$mJvcIQRJrxdQHafEVKaWK766DqgXS8GNMcNAp2A8qhm3Jid6i1gpEwYyiRF/hu00BV8wciPmh99H5L7vVM5oi0";
  users.users.wired = {
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" ];
    initialHashedPassword = "$6$KwTBefwXsq1bFqjv$bBSBZsiaQWNiCdwmna0ApvZMyejuxshp4IbsjFB8OoyzO/SUpJ6L6N9yUfMTITZyjo8HB2n//lguS6Ck1AeL41";
    # packages = with pkgs; [ ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim
    wget
    git
    htop
  ];
  
  security.apparmor.enable = true;
  security.doas.enable = true;
  security.sudo.enable = false;
  # Configure doas
  security.doas.extraRules = [{
  users = [ "wired" ];
  keepEnv = true;
  persist = true;  
  }];
  
  environment.variables.EDITOR = "vim";
  
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  
  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  # system.stateVersion = "22.11"; # Did you read the comment?
 
}

